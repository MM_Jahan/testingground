// Fill out your copyright notice in the Description page of Project Settings.


#include "ChooseNextWaypoint.h"
#include "AIController.h"
#include "PatrolRouteComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
//#include "BehaviorTree/BTTaskNode.h"

EBTNodeResult::Type UChooseNextWaypoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    //GetPatrol route
    auto ControlledPawn = OwnerComp.GetAIOwner()->GetPawn();
    auto PatrolRoute = ControlledPawn->FindComponentByClass<UPatrolRouteComponent>();

    if (!ensure(PatrolRoute))
        {return EBTNodeResult::Failed;}

    // get patrol point
    auto PatrolPoints = PatrolRoute->GetPatrolPoints();
    if(PatrolPoints.Num() == 0)
    {
        UE_LOG(LogTemp, Error, TEXT("A guerd is missing patrol points"))
        return EBTNodeResult::Failed;
    }

    // Set Next Way point
    auto BlackboardComp = OwnerComp.GetBlackboardComponent();
    int32 Index = BlackboardComp->GetValueAsInt(IndexKey.SelectedKeyName); // Gets the key value as int that is adresed to the IndexKey vaiable
    BlackboardComp->SetValueAsObject(WaypointKey.SelectedKeyName, PatrolPoints[Index]);

    //Cycle index
    auto NextIndex = ++Index % PatrolPoints.Num();
    BlackboardComp->SetValueAsInt(IndexKey.SelectedKeyName, NextIndex);
    
    
    //Addrese the blackboard on this begaviour tree
    //UE_LOG(LogTemp, Display, TEXT("way pint intex: %i"), Index)
    return EBTNodeResult::Succeeded;
}